import React from 'react';

type FilterProps = {
    onFiltersData: (value: string) => void;
};


/**
 * Build filters to apply to the list component
 */
const GridFilters = ({ onFiltersData } : FilterProps) => {
    const [year, setYear] = React.useState<string>();
    const onSelectYear = (event: React.FormEvent<HTMLSelectElement>) => {
        const { value } = event.currentTarget;
        setYear(value);
        onFiltersData(value);
    };

    return (
        <div className="grid-filters">
            <label htmlFor="year">Filtrer par année</label>
            <select name="year" id="year" onChange={onSelectYear} value={year}>
                { 
                    ['', 2019, 2020, 2021]
                        .map(year => (
                            <option key={year} value={year}>{year === '' ? 'Tous' : year}</option>
                        ))
                }
            </select>
        </div>
    );
};

export default GridFilters;
