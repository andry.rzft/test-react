import React from 'react';

type MessageProps = {
    message: string | null
};

/**
 * Display a customized message when error occured
 */
const ErrorPage = ({ message } : MessageProps): JSX.Element => {
    return (
        <div className="page-error">
            <h2>Erreur</h2>
            <p>{message !== null ? message :  'Une erreur s\'est produite' }</p>
        </div>
    );
};

export default ErrorPage;
