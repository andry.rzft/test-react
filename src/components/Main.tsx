import React from 'react';
import { getAvailablePod } from '../common/services';
import { PodType } from '../common/types';
import ErrorPage from './ErrorPage';
import PodDataView from './PodDataView';

/**
 * Main container for the Pod data view
 */
const Main  = (): JSX.Element => {
    const [podTypes, setPodTypes] = React.useState<PodType[] | null>(null);
    const [errorMessage, setMessageError] = React.useState<string | null>(null);

    React.useEffect(() => {
        getAvailablePod()
            .then((response) => {
                setMessageError(response.errMessage);
                setPodTypes(response.data);
            });
    }, []);

    if (errorMessage !== null) {
        return ( <ErrorPage message={errorMessage} /> );
    }

    return ( <PodDataView data={podTypes} /> );
};

export default Main;
