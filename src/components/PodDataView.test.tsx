import PodDataView from './PodDataView';
import { mount, shallow } from 'enzyme';
import { PodType } from '../common/types';
import PodList from './PodList';

describe('Render <PodDataView />', () => {
    const dataFromServer = [{
        createdAt: "2021-12-08T08:58:31.521Z",
        energy: "electricity",
        pointOfDelivery: "183920467389645"
    }, {
        createdAt: "2021-12-08T03:34:12.961Z",
        energy: "gas",
        pointOfDelivery: "226378926478926"
    }] as PodType[];

    it('It should loading view', () => {
        expect(
            mount(<PodDataView data={dataFromServer}/>)
            .find('PodList')
        );
    });
});
