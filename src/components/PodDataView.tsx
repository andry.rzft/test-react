import React from 'react';
import { PodType } from '../common/types';
import ErrorPage from './ErrorPage';
import PodList from './PodList';
import NoDataPage from './NoDataPage';
import TableView from './TableView';
import GridFilters from './GridFilters';
import { usePodData } from './usePodData';

type PodDataProps = {
    data: PodType[] | null;
};

/**
 * Get the translation of the energy type
 * @param podType 
 * @returns {string}
 */
const getEnergyType = (podType: string | null)  => {
    let energy = '';
    
    if (podType === 'electricity') {
        energy = 'd\'électricité';
    } else if (podType === 'gas') {
        energy = 'de gaz';
    }

    return energy;
};

/**
 * Display information about the gas or electricity consumption for users in a grid
 * @param data
 * @return {JSX.Element}
 */
const PodDataView = ({ data=  [] }: PodDataProps): JSX.Element => {
    const [podId, setPodId] = React.useState<string | null>(null);
    const [podType, setPodType] = React.useState<string | null>(null);
    const [filterValue, setFilterValue] = React.useState<string>('');
    const { mainData, errorMessage } = usePodData(podId);

    const clickFilterFn = (type: string, pointOfDelivery: string) => {
        setPodId(pointOfDelivery);
        setPodType(type);
    };
    
    const filterData = (value: string) => {
        setFilterValue(value);
    };

    // Select the first type of electricity if it exists
    React.useEffect(() => {
        if (data && data.length > 0) {
            clickFilterFn(data[0].energy, data[0].pointOfDelivery);
        }
    }, [data]);

    if (data?.length === 0) {
        return (<NoDataPage message= "Pas de POD disponible pour le moment" />);
    }

    if (errorMessage && errorMessage.length > 0) {
        return ( <ErrorPage message={errorMessage} /> );
    }

    const gridTitle = `Liste de consommations ${getEnergyType(podType)} ${filterValue !== '' ? `pour l'année ${filterValue}`: ''}`;

    return (
        <>
            <PodList data={data} onClickFilterFn={clickFilterFn} />
            <GridFilters onFiltersData={filterData} />
            <TableView gridTitle={gridTitle} data={mainData} filters={filterValue}/>
        </>
    );
};

export default PodDataView;
