import React from 'react';
import { PodElectricity, PodGas } from '../common/types';
import NoDataPage from './NoDataPage';

type TableViewProps =  {
    filters: string;
    gridTitle: string;
    data: PodElectricity[] | PodGas[];
};

/**
 * Extracts table's headers from the data
 * @param data 
 * @return {String[]} the headers
 */
const extractHeadersFromData = (data: PodElectricity[] | PodGas[]) => {
    const [oneItem] = data;
    return Object.keys(oneItem || {}).filter(item => item !== 'id');
};

/**
 * Format the value to match with the type of this value
 * @param header 
 * @param value 
 * @returns 
 */
const formatValue = (header: string, value: string) => {
    switch (header)  {
        case 'date':
            return new Date(value).toLocaleDateString();
        default:
            return value;
    }
};

/**
 * Formats string to get the real name of the colmun's header
 * @param header
 * @param onButtonClick
 * @return {String} the formatted header to display
 */
const formatHeader = (header: string, onButtonClick: void) => {
    switch (header)  {
        case 'date':
            return 'Date de relevé';
        case 'valueHP':
            return 'Heure pleine (kWh)';
        case 'valueHC':
            return 'Heure creuse (kWh)';
        case 'value':
            return 'Index (kWh)'
        default:
            return header;
    }
};

/**
 * Filters the data before populating on the grid
 * @param tableData the data to display
 * @param value the filter value
 * @returns 
 */
const filterData = (tableData: PodElectricity[] | PodGas[], value: string) => {
    if (value && value.length > 0) {
        return [
            ...(tableData as any[]).filter((pod: PodElectricity | PodGas) => new Date(pod.date).getFullYear() === parseInt(value, 10))
        ];
    }
    return tableData;
};

/**
 * Build the grid with its data
 */
const TableView = ({ data = [], gridTitle, filters }: TableViewProps): JSX.Element=> {
    const dataToDisplay = filterData(data, filters);
    if (!dataToDisplay  || dataToDisplay.length === 0) {
        return (<NoDataPage message= "Pas de données disponible pour ce POD pour le moment" />);
    }

    const headers = extractHeadersFromData(dataToDisplay);

    return (
        <table className="data-table">
            <caption>{gridTitle}</caption>
            <thead>
                <tr>
                {
                    headers.map(header => (
                        <th key={header}>{formatHeader(header)}</th>
                    ))
                }
                </tr>
            </thead>
            <tbody>
            {
                dataToDisplay.map((item: any , index: number) => (
                        <tr key={index} className={index % 2 === 0 ? 'even-row' : undefined} >
                        { 
                            headers.map((header: string, idx: number) => (
                                <td key={idx}>
                                { formatValue(header, item[header]) }
                                </td>
                            ))
                        }
                        </tr>
                    )
                )
            }
            </tbody>
        </table>
    );
};

export default TableView;
