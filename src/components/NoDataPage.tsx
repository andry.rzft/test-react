import React from 'react';

type NoDataProps = {
    message: string;
};

/**
 * Display a message in case of an empty data
 */
const NoDataPage = ({ message }: NoDataProps) => {
    return (
        <div className="no-data">
            { message }
        </div>
    );
};

export default NoDataPage;
