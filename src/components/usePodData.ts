import React from 'react';
import { getPodDataById } from '../common/services';
import { PodElectricity, PodGas } from '../common/types';

/**
 * Hooks to handle the data to display in the grid depending on the type of electricity
 * @param podId the type of electricity gas | electricity
 * @returns 
 */
export const usePodData =  (podId: string | null) => {
    const [mainData, setMainData] = React.useState<PodElectricity[] | PodGas[]>([]);
    const [errorMessage, setErrorMessage] = React.useState<string | null>('');
    React.useEffect(() => {
        const getDataById = () => {
            if (podId !== null) {
                getPodDataById(podId)
                    .then(res => {
                        setMainData(res.data);
                        setErrorMessage(res.errMessage);
                    });
            }
        };
        getDataById();
    }, [podId]);

    return { mainData, errorMessage };
};
