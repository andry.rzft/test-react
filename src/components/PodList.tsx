import React from 'react';
import { PodType } from '../common/types';

type FilterProps = {
    data: PodType[] | null;
    onClickFilterFn: (type: string, pointOfDelivery: string) => void;
};

/**
 * Build a filter for managing the view of the data consumption made by users for electricity or gas
 */
const PodList = ({ data, onClickFilterFn }: FilterProps): JSX.Element => {
    const [active, setActive] = React.useState<string>();

    const onButtonClick = (e: React.MouseEvent<HTMLButtonElement>, item: PodType) => {
        e.preventDefault();
        setActive(e.currentTarget.id);
        onClickFilterFn(item.energy, item.pointOfDelivery);
    };

    return (
        <div className="pod-type">
            { 
                data?.map((item: PodType, index: number) => {
                    if (active === undefined && index === 0) {
                        setActive(item.pointOfDelivery);
                    }
                    return (
                        <button 
                            key= {item.pointOfDelivery}
                            className= {active === item.pointOfDelivery ? 'active' : undefined}
                            id={item.pointOfDelivery}
                            onClick={(e) => onButtonClick(e, item)}
                        >
                            {item.energy}
                        </button>
                    );
                }
            )}
        </div>
    );
};

export default PodList;
