export type PodType = {
    createdAt: string;
    energy: 'electricity' | 'gas';
    pointOfDelivery: string;
};

export type responseData = {
    errMessage: string | null;
    data: any;
};

export interface MainPodType {
    date: string;
    id:  number;
}

export interface PodElectricity extends MainPodType  {
    valueHC: number;
    valueHP: number;
};

export interface PodGas extends MainPodType {
    value: number; 
};
