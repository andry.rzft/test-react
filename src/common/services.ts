import { BASE_URL, COMPTEUR_DESIGNATION } from "./constants";
import { responseData } from "./types";

/**
 * Create a generic method to fetch data from a given endpoint
 * @param type 
 * @param errorMessage 
 * @returns {Promise<any>}
 */
const applyFetch = (type: String): Promise<responseData> => {
    return fetch(`${BASE_URL}${type}`)
    .then(response => {
        if (response.ok) {
            return response;
        } else {
            const error = new Error(`Code d'erreur ${response.status}: ${response.statusText}`);
            throw error;
        }
    }, err => {
        const error = new Error(err.message);
        throw error;
    })
    .then(res => res.json())
    .then(res => ({
        errMessage: null,
        data: res
    }))
    .catch(error => ({
        errMessage: error.message,
        data: null
    }));
};

/**
 * Get POD type and its informations
 * @returns {Promise<any>}
 */
export const getAvailablePod = (): Promise<responseData> => {
     return applyFetch(COMPTEUR_DESIGNATION);
};

/**
 * Get data for a specific POD
 * @param id 
 * @returns {Promise<any>}
 */
export const getPodDataById = (id: string): Promise<responseData> => {
    return applyFetch(id);
};
